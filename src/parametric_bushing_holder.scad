// Universal bushing mount by Bill Ruckman
// Modifications by Free Beachler, Terawatt Industries
// - round edges, thinner profile

module BushingMount(){

bushingHeight = 16;
mountingHole = 5;
mntSlotWidth = 6;
mntSlotSeparation = 36;
mntBaseLen = 55;
mntBaseThk = 4;

shaftCenter = 12;  //distance from base to center of shaft
shaftDiameter = 8;
holeDiameter = 16.1;
round_edge_rad_support = 2;
round_edge_rad_mnt_arms = 0;

wallThk = 3;  //minimum around hole

horizontalDowels = false;
verticalDowels = false;

dowelDiameter = 3.2;
dowelDepth = holeDiameter + (wallThk * 2);

clearance = .1; // for dowel pin edges to shaft

module holeSlot() {
	for(x=[mntSlotWidth/3, -mntSlotWidth/3]) {
		translate([x,0,0]) rotate([90,0,0]) cylinder(r=mountingHole/2, h=mntBaseThk + .1, center=true, $fn=20);
	}
	cube([mntSlotWidth, mntBaseThk + 0.1, mountingHole], center=true);
}

module dowels() {
	//horizontal dowel holes
	if(horizontalDowels){
		translate([shaftDiameter/2-wallThk,-(shaftCenter+shaftDiameter/2+dowelDiameter/2+clearance),0]) rotate([0,90,0])cylinder(r=dowelDiameter/2+.1, h=dowelDepth, center=true, $fn=35);
		translate([shaftDiameter/2-wallThk,-(shaftCenter-shaftDiameter/2-dowelDiameter/2-clearance),0]) rotate([0,90,0])cylinder(r=dowelDiameter/2+.1, h=dowelDepth, center=true, $fn=35);
	}
	//vertical dowel holes
	if(verticalDowels){
		translate([(shaftDiameter)/2+dowelDiameter/2+clearance,-(shaftCenter+shaftDiameter/2-1),0]) rotate([90,0,0])cylinder(r=dowelDiameter/2+.1, h=dowelDepth, center=true, $fn=35);
		translate([-(shaftDiameter)/2-dowelDiameter/2-clearance,-(shaftCenter+shaftDiameter/2-1),0]) rotate([90,0,0])cylinder(r=dowelDiameter/2+.1, h=dowelDepth, center=true, $fn=35);
	}
}

//mounting arms
difference(){
	minkowski()
	{
	cube([mntBaseLen,mntBaseThk - 2*round_edge_rad_mnt_arms, bushingHeight - 2*round_edge_rad_mnt_arms], center=true);
	translate([0,0,0]) 
	rotate([0,90,0])
	cylinder(r=round_edge_rad_mnt_arms, h=0.01, center=false, $fn=35);
	}
	for(a= [mntSlotSeparation/2, -mntSlotSeparation/2]){
		translate([a,0,0]) 
			holeSlot();
	}
	// cut into mounting arms for large hole dia.
	translate([0,-(shaftCenter),0]) 
		cylinder(r=holeDiameter/2, h=bushingHeight + 1, center=true, $fn=35);
}

//bushing support
difference(){
	//body
union() {
	minkowski() {
	union(){
		translate([0,-(shaftCenter/2),0]) 
			cube([holeDiameter+wallThk*2 - 2*round_edge_rad_support - 0.1, shaftCenter  - 2*round_edge_rad_support - mntBaseThk/2, bushingHeight - 2*round_edge_rad_support], center=true);
		translate([0,-(shaftCenter),0]) 
			cube([holeDiameter+wallThk*2 - 2*round_edge_rad_support, holeDiameter+wallThk*2 - 2*round_edge_rad_support - mntBaseThk/2, bushingHeight - 2*round_edge_rad_support], center=true);
	}
	translate([0,0,0]) 
	rotate([0,90,0])
	cylinder(r=round_edge_rad_support, h=0.01, center=false, $fn=35);
	rotate([0,0,0])
	cylinder(r=round_edge_rad_support, h=0.01, center=false, $fn=35);
	}
	translate([0,-mntBaseThk,0]) 
		cube([holeDiameter+wallThk*2, mntBaseThk, bushingHeight], center=true);

}
	//hole
	translate([0,-(shaftCenter),0]) cylinder(r=holeDiameter/2, h=bushingHeight + 0.1, center=true, $fn=35);
	dowels();
}

}

BushingMount();
